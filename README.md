![image](src/main/resources/tachyonis.space.jpg)

# Spring Boot Greenwich SR4 KafKa Producer/Consumer Clients

This is an app to buffer Microservices with OATH2 enpoints from NodeJS or KafKA Producers to Topic Email

* TLS/SSL Microservices end point from PK12 Java self signed keystore
* Handle POST Email to KafKa Topics at a perfomance of 10000 messages/second 
* SMTP Email from Producers
* KAFKA Confluent 3.5.1 Docker setup with 3 brokers
* KAFKA Consumers configurable threads group
* Configurable SMTP gateway
* Single One jar solution using spring-boot-maven-plugin
* Microsoft Windows [Version 6.1.7601]
* Java(TM) SE Runtime Environment (build 1.8.0_231-b11)
* Java HotSpot(TM) 64-Bit Server VM (build 25.231-b11, mixed mode)

# Run the Application
java -Xmx256M -jar GreenwichSR4-0.0.1-spring-boot.jar

## DISCLAIMER

This application is for reference purposes only.

## Usage

Option 1: Edit appContext.xml


## Pre-requisites

* Oracle VM 6.0
* Redhat 64 bit ISO 7.7
* KafKa Docker 3.5.1 setup on Redhat
* yum install git, JDK 1.8, wget
* Docker EE and Docker Compose
* `mvn` installed on Windows 64 bit 7/10 

## Eclipse
![image](src/main/resources/spring.png)

## RedHat in Oracle VM
![image](src/main/resources/redhat.PNG)

## Docker Start from KafKA using cp-all-in-one with modified docker-compose.yml

Replace the original downloaded with the modified in the same folder as this README.md.
 ![image](src/main/resources/kafka.PNG)