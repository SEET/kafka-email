var express = require('express')
var router = express.Router()
var fs = require('fs')
var bodyParser = require('body-parser')
var path = require('path')
var filename = path.basename(__filename)
var filename = filename.substring(0, filename.length-3)
var Client = require('node-rest-client').Client

router.use(function timeLog ( req, res, next ) {
 next()
})

// parse application/x-www-form-urlencoded
//router.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
//router.use(bodyParser.json())
router.use( bodyParser.raw() )

router.get('/about', function (req, res) {
 res.send('Email REST Gateway')
})

router.post('/', function (req, res ) {
    //res.send(req.body)
    var client = new Client()
    //	JSON.parse(req.body, (key, value) => {
    //		console.log("KEY@@@@@@@",key,"@@@@@@@@")
    //})
    console.log('@@@ Body @@@@@@', req.body, "@@@@@@@")
    var args = {
     data: {"value_schema": "{\"type\": \record\", \"name\": \"AvroEml\", \"namespace\":\"tachyonis.space\", \"fields\": [{\"name\": \"Id\",\"type\": \"string\"},{\"name\": \"sender\",\"type\": \"string\"},{\"name\": \"receiver\",\"type\": \"string\"},{\"name\": \"subject\",\"type\": \"string\"},{\"name\": \"sendDate\",\"type\": \"string\"},{\"name\": \"body\",\"type\": \"string\"},{\"name\": \"retry\",\"type\": [\"int\",\"null\"]},{\"name\": \"license\",\"type\": [\"string\",\"null\"]}]}", "records":[ {"Id": "12345"},
     { "sender":"seethinglong@yahoo.com"},
     { "receiver": "seethinglong@gmail.com"},
     { "subject":"Sub"},
     { "sendDate":"i234567890"},
     { "body": "Body 123"},
     { "retry":"3"},
     { "license":"" }]},
     headers: { "Content-Type": "application/vnd.kafka.json.v2+json" }
    }
   

    var now = new Date()
    var start = new Date(now.getFullYear(), 0, 0)
    var diff = (now - start) + ((start.getTimezoneOffset() - now.getTimezoneOffset()) * 60 * 1000)
    var oneDay = 1000 * 60 * 60 * 24
    var day = Math.floor(diff / oneDay)
    var hours = now.getHours()
    var kafka_topic = "EMAIL_" + day + "_" + hours
    var uri_topic = 'http://172.16.0.214:8082/topics/' + kafka_topic;
    console.log('@@@ Sending to ' , uri_topic )

    client.post(uri_topic, args, function (data, response) {
       //console.log(data);
       // raw response
       res.send(JSON.stringify({"statusCode":response.statusCode, "statusMessage":response.statusMessage})); 
       console.log(JSON.stringify({"statusCode":response.statusCode, "statusMessage":response.statusMessage})); 
       console.log('Text Upload successful! Server responded ')
    })
})
module.exports = router
