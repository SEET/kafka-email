package tachyonis.space.kafka;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;

import org.apache.avro.generic.GenericData.Record;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import tachyonis.space.AvroEml;
import tachyonis.space.Config;
import tachyonis.space.email.EmailService;

import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.Properties;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class MailConsumer implements Runnable {
	
	Logger log = LogManager.getLogger(MailConsumer.class);
	//private final String CONSUMER_APP_ID = getClass().getName();
	
	private static String TOPIC;
    private static String BOOTSTRAP_SERVERS_CONFIG;
    private static String SCHEMA_REGISTRY_URL_CONFIG;
    private static String CTX;
    private static EmailService mailer;
    private static Config config;

    private static boolean loop = true;
    public void stop(){
    	loop= false;
    	//Thread.currentThread().isInterrupted();
    }
    public MailConsumer(EmailService mailer, Config c){  
    	MailConsumer.mailer = mailer;
    	MailConsumer.TOPIC = "EMAIL";
        MailConsumer.BOOTSTRAP_SERVERS_CONFIG = "kafka:19092,kafka:29092,kafka:39092";
        MailConsumer.SCHEMA_REGISTRY_URL_CONFIG = "http://kafka:8081";
        MailConsumer.CTX = "./appContext.xml";
        MailConsumer.config = c;
    }  
    
	@Override
    public void run() {
        final Properties props = new Properties();
        loop = true;
		String h = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		String l_topic = TOPIC + "_" + String.valueOf(LocalDate.now().getDayOfYear()) + "_" + h;
        
        //String m = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
        
        //Grouping Strategies
        final String s = String.valueOf(Thread.currentThread().getId());
        final String clientId   = s;
        final String instanceId = new SimpleDateFormat("HHmmss").format(Calendar.getInstance().getTime())+"-"+clientId;
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS_CONFIG);
        
        props.put(ConsumerConfig.GROUP_INSTANCE_ID_CONFIG, instanceId );
        props.put(ConsumerConfig.CLIENT_ID_CONFIG,clientId );
        props.put(ConsumerConfig.GROUP_ID_CONFIG, TOPIC );
        
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, SCHEMA_REGISTRY_URL_CONFIG);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true); 
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG,"6000");
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG,"9000");
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "3000");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);
        //props.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG ,"1000");
        
        KafkaConsumer<String, AvroEml> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(l_topic));
        MailProducer producer = new MailProducer();
        try {
        	int partition=0;
        	long offset=1L;
        	int poll_count = 0;
        	while (loop) {
        	try {
            	ConsumerRecords<String, AvroEml> records = consumer.poll(Duration.ofMillis(100));
        		if( records.count() == 1 ) {
        			log.info("@@@ Number of records = " + String.valueOf(records.count()));
        			for (ConsumerRecord<String, AvroEml> record : records) {
        				AvroEml value = record.value();
        				Assert.notNull(value, "Record Value Empty");
        				Assert.notNull(value.getSender(),"Email Sender Empty");
        				Assert.notNull(value.getReceiver(), "Email Recipient Empty");
        				Assert.notNull(value.getSubject(), "Email Subject Empty");
        				Assert.notNull(value.getBody(), "Email Body Empty");
                       
        				// log.info( value.getSendDate() + " Sending from " + value.getSender()  + " to " + value.getReceiver() + " Subject : " + value.getSubject());
        				// Email takes slower than 500ms and exceed the Polling of Consumer instance Polling.  
        				// Seet Hing Long 2019 Nov 18
        				// Consumer not Thread safe.  Cannot implement Thread within consumer object
        				
        				//new Thread() {
        				//    @Override
        				//    public void run() {
      							partition=record.partition();
      							offset=record.offset();
        				    	log.info("P:O=" + record.partition() + ":" + record.offset() + " Executed " + value.getReceiver() + " Attachement=" + value.getAttachment());
        				    	//consumer.commitAsync();
        				    	//  Simulate 200miliseconds seconds sendmail latency 
        				    	//Thread.sleep(200);
        				    	try {
        				    	mailer.sendMailWithAttachment(
        				    			value.getSender(),
        				    			value.getReceiver(), 
        				    			value.getBcc(),
        				    			value.getCc(),
        				    			value.getSubject(),
        				    			value.getBody(), 
        				    			value.getAttachment(),
        				    			value.getPath());
        				    	// The records must be one only set by consumer MAX_POLL_RECORDS_CONFIG
        				    	consumer.commitSync();
        				    	poll_count+=60;
        				    	//mailer.sendMailchar(value.getReceiver(),value.getSubject(),value.getBody());
        				    	}catch(MailException e){
        				    		String t_topic = l_topic + "_ERROR";
        				    		producer.insert(config, value, t_topic);
        				    	}
        				}
        			}else if(records.count() >1) {
        				log.error("@@@ Consumer return more than 1 records " + String.valueOf(records.count()));
        			}
        		} catch (SerializationException e) {
        			String[] error = e.getMessage().split(" ");
        			String[] topic_partition = error[5].split("-");
        			TopicPartition p = new TopicPartition(topic_partition[0], Integer.valueOf(topic_partition[1]));
        			offset = Long.valueOf(error[8]);
        			consumer.seek(p, offset); 
        			log.error("Topic = " + topic_partition[0] + "Partition = " + topic_partition[1] );
        			log.error("Offset = "+ error[8]);
        			log.error(e.getMessage());
        		} catch (IllegalArgumentException e) {
        			consumer.commitSync();
        			TopicPartition p = new TopicPartition(TOPIC,partition);
        			offset = offset +1;
        			log.info("Seek pass Partition " + partition + " Offset " +  offset);
                	consumer.seek(p, offset);
                	log.error(e.getMessage());
        		}finally {
        			poll_count++;
        			if ( poll_count > 3600 ) {
        				consumer.close();
        				loop = false;
        			}
        		}
        	}
        } catch (Exception e) {
    		log.error(e.getMessage());
    		consumer.close();
    	} finally {
        	loop=false;
    	}
    }
}