package tachyonis.space.kafka;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import tachyonis.space.AvroEml;
import tachyonis.space.Config;

import org.apache.kafka.common.errors.SerializationException;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MailProducer {
	Logger log = LogManager.getLogger(MailProducer.class);
    private static String BOOTSTRAP_SERVERS_CONFIG = "localhost:9092";
    private static String SCHEMA_REGISTRY_URL_CONFIG = "http://localhost:8081";
    final Properties props = new Properties();
    static boolean status = false;

    public void run(final Config c) {
    	String TOPIC = c.getTopic() + "_" + String.valueOf(LocalDate.now().getDayOfYear());
    	TOPIC = TOPIC + new SimpleDateFormat("HH").format(Calendar.getInstance().getTime()) ;
    	SCHEMA_REGISTRY_URL_CONFIG = c.getSchema_registry_url_config();
        //Get the mailer instance
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, (String) c.getBootstrap_servers_config());
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, (String) c.getSchema_registry_url_config());
   }
    
    public void test(final Config c, AvroEml eml, String TOPIC) {
        try (KafkaProducer<String, AvroEml> producer = new KafkaProducer<String, AvroEml>(props)) {
            for (long i = 0; i < 99; i++) {
                eml.setId(UUID.randomUUID().toString());
                eml.setSender("seethinglong@yahoo.com");
                eml.setSubject(String.valueOf(i) + ".  Event Is Coming Soon");
                eml.setReceiver("seethinglong@gmail.com");
                eml.setBody("Fire holds off Hong Kong police at protest campus");
                 Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                eml.setSendDate(formatter.format(date));
                List<CharSequence> a = new ArrayList<CharSequence>();
                a.add("tachyonis.space.jpg");
                eml.setAttachment(a);
                final ProducerRecord<String, AvroEml> record = new ProducerRecord<String, AvroEml>(TOPIC, null, eml);
                producer.send(record);
            }
            producer.flush();
            log.info("Successfully produced 100 messages to a topic called " + TOPIC);

        } catch (final SerializationException e) {
            log.error(e.getMessage());;
        }
    }
    
    public boolean insert(final Config c, AvroEml eml, String TOPIC) {
		String h = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		String l_topic = TOPIC + "_" + String.valueOf(LocalDate.now().getDayOfYear()) + "_" + h;
		status = true;
        try (KafkaProducer<String, AvroEml> producer = new KafkaProducer<String, AvroEml>(props)) {
                final ProducerRecord<String, AvroEml> record = new ProducerRecord<String, AvroEml>(l_topic, null, eml);
                log.info("KafKa producer send record UUID=" + eml.getId() );
                producer.send(record,
                		new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if(e != null) {
                        	status = false;
                        	log.error(eml.getId().toString() + " Failed");
                        	log.error(e.getMessage());
                        } else {
                        	status = true;
                        	log.info("Successful.  The offset of the record:" + metadata.offset());
                        	
                        }
                    }
                });
        } catch (final SerializationException e) {
            log.error(e.getMessage());
            status = false;
        }
        return status;
    }
    
    public boolean batchinsert(final Config c, List<AvroEml> eml_list, String TOPIC) {
		String h = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		String l_topic = TOPIC + String.valueOf(LocalDate.now().getDayOfYear()) + "_" + h;
		status=false;
    	for (AvroEml eml : eml_list) {
	        try (KafkaProducer<String, AvroEml> producer = new KafkaProducer<String, AvroEml>(props)) {
	                final ProducerRecord<String, AvroEml> record = new ProducerRecord<String, AvroEml>(l_topic, null, eml);
	                producer.send(record,
	                		new Callback() {
	                    public void onCompletion(RecordMetadata metadata, Exception e) {
	                        if(e != null) {
	                           log.error(e.getMessage());
	                           
	                        } else {
	                           log.info("Record UUID=" + eml.getId());
	                           log.info("The offset of the record:" + metadata.offset());
	                           status=true;
	                        }
	                    }
	                });
	                
	        } catch (final SerializationException e) {
	            log.error(e.getMessage());
	            status = false;
	        }
    	}
    	
        log.info("@@@ Insert record KafKa total = " + eml_list.size() );
        return status;
    }
}

