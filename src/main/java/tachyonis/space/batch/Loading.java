package tachyonis.space.batch;

import java.io.File;
import java.io.IOException;
import static java.nio.file.StandardCopyOption.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tachyonis.space.AvroEml;
import tachyonis.space.Config;
import tachyonis.space.JsonEml;
import tachyonis.space.kafka.MailConsumer;
import tachyonis.space.kafka.MailProducer;

public class Loading implements Runnable {
	static Logger log = LogManager.getLogger(Loading.class);
	private static Config config;
	private static boolean status;

    public Loading(Config c){ 
    	Loading.config = c;
    }  

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
	private static boolean avrotoproducer(AvroEml eml) {
		MailProducer p = new MailProducer();
		Report r = new Report(Loading.config);
		new Thread() {
		@Override
	    public void run() {
	    		try {
					p.run(Loading.config);
					status = p.insert(Loading.config, eml, "EMAIL");
					if (status) {
						log.info("KafKa Inserted new record");
					}else {
						log.info("KafKa Error");
					}
				} catch (Exception e) {
					log.error(e.getMessage());
					status = false;
				}
	    	}
		}.start();
		try {
			log.info("Wait for Producer to insert record");
			Thread.sleep(369);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
		if (status){
			log.info("avrotoproducer Producer End Thread Successful");
		}else {
			log.info("avrotoproducer Producer End Thread Failed");
		}
		return status;
	}
	
    public static void jsontoavro(String fileName) {
    	ObjectMapper objectMapper = new ObjectMapper();
    	
    	File file = new File(fileName);
    	if ( file.exists()) {
	    	AvroEml eml = new AvroEml();
	    	try {
				JsonEml json = objectMapper.readValue(file, JsonEml.class);
				eml.setId(UUID.randomUUID().toString());
				eml.setAttachment(json.getAttachment());
				eml.setBody(json.getBody());
				eml.setReceiver(json.getReceiver());
				eml.setSendDate(json.getSendDate());
				eml.setSender(json.getSender());
				eml.setSubject(json.getSubject());
				eml.setPath(json.getPath());
				Validate v = new Validate();
				if ( v.AllAttachmentExist(json.getAttachment(),json.getPath())) {
					avrotoproducer(eml);
				}
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				log.error("Loading File Failed : " + fileName);
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				log.error("Loading File Failed : " + fileName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error("Loading File Failed : " + fileName);
			}
    	}
    }
    
    public static boolean jsontoavroList(String path, Set<String> fileNames) {
    	ObjectMapper objectMapper = new ObjectMapper();
    	int processed_file_count = 0;
    	//String h = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		//String y_h = String.valueOf(LocalDate.now().getDayOfYear()) + "_" + h;
    	String y_h = "Backup";
    	
    	for (String fileName : fileNames) {
	    	String f = Paths.get(fileName).getFileName().toString();
	    	String separator = "/";
    		String OS = System.getProperty("os.name").toLowerCase();
		    if (OS.indexOf("win") >= 0) {
		    	separator = "\\";
		    }
		    //fileName is full path
		    Path frfileName = Paths.get(fileName);
	    	File directory = new File(path + separator + y_h + separator + "Success");
	    	if (! directory.exists()){
		        directory.mkdir();
		        log.debug("Created Folder " + directory);
		    }else {
		    	log.debug("Folder Exist " + directory);
		    }
	    	Path tofileName = Paths.get(directory + separator + f);
	    	
	    	directory = new File(path + separator + y_h + separator + "Fail");
		    if (! directory.exists()){
		        directory.mkdir();
		        log.debug("Created Folder " + directory);
		    }else {
		    	log.debug("Folder Exist " + directory);
		    }
		    
    		File file = new File(fileName);
    		
    		if ( file.exists()) {
	    		AvroEml eml = new AvroEml();
		    	try {
					JsonEml json = objectMapper.readValue(file, JsonEml.class);
					eml.setId(UUID.randomUUID().toString());
					eml.setAttachment(json.getAttachment());
					//Converting String Body into Character Sequence because of HTML
					CharSequence cs = new StringBuffer(json.getBody());
					eml.setBody(cs);
					eml.setReceiver(json.getReceiver());
					eml.setBcc(json.getBcc());
					eml.setCc(json.getCc());
					eml.setSendDate(json.getSendDate());
					eml.setSender(json.getSender());
					eml.setSubject(json.getSender());
					eml.setPath(json.getPath());
					Validate v = new Validate();
					if ( v.AllAttachmentExist(json.getAttachment(),json.getPath())) {
						if (avrotoproducer(eml)) {
							log.info("Loading to KafKa Completed : " + fileName);
							log.info("Move File From : " + frfileName.toString() + " to " + tofileName.toString());
							Files.move(frfileName, tofileName, REPLACE_EXISTING);
							//SMTP Email GW need to attach files after consume.  Cannot move the attachments
							//v.AllAttachmentBackup(json.getAttachment(),path);
							processed_file_count ++;
						}else {
							log.info("Loading to KafKa Failed : " + fileName);
							Path FailfileName = Paths.get(path + separator + y_h + separator + "Fail" + separator + f + ".KafKaError");
							Files.move(frfileName, FailfileName, REPLACE_EXISTING);
						}
					}else {
						log.error("No loading " + fileName);
						Path FailfileName = Paths.get(path + separator + y_h + separator + "Fail" + separator + f + ".AttachmentValidateError");
						log.error("Move File From : " + frfileName.toString() + " to " + FailfileName.toString());
						Files.move(frfileName, FailfileName, REPLACE_EXISTING);
					}
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					log.error(" JsonParseException Loading File Failed : " + fileName);
					log.error(e.getMessage());
					try {
						Path FailfileName = Paths.get(path + separator + y_h + separator + "Fail" + separator + f + ".JsonParseException");
						Files.move(frfileName, FailfileName, REPLACE_EXISTING);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						log.error(e1.getMessage());
					}
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					log.error("JsonMappingException Loading File Failed : " + fileName);
					log.error(e.getMessage());
					try {
						Path FailfileName = Paths.get(path + separator + y_h + separator + "Fail" + separator + f + ".JsonMappingException");
						Files.move(frfileName, FailfileName, REPLACE_EXISTING);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						log.error(e1.getMessage());
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					log.error("IOException Loading File Failed : " + fileName);
					log.error(e.getMessage());
					try {
						Path FailfileName = Paths.get(path + separator + y_h + separator + "Fail" + separator + f + ".IOException");
						Files.move(frfileName, FailfileName, REPLACE_EXISTING);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						log.error(e1.getMessage());
					}
					
				}
	    	}else {
	    		log.error("Loading Failed.  File not exist " + fileName );
	    	}
    	}
    	float rate = ( processed_file_count/ fileNames.size() ) * 100;
    	log.info("Total Processes Success " + String.valueOf(processed_file_count));
    	log.info("Total Unique Files " + String.valueOf(fileNames.size()));
    	log.info("Total Success Rate " + String.valueOf(rate));
    	return true;
    }
}
