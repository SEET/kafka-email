package tachyonis.space.batch;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import static java.nio.file.StandardCopyOption.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.CopyOption;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import tachyonis.space.Config;
import org.apache.logging.log4j.LogManager;

public class Validate {
	static Logger log = LogManager.getLogger(Validate.class);

	public Validate() {
		log.info("Validating File");
	}
	
   //TODO implement Batch validations
   public boolean AllfilesExist( FileList f, String path) 
	  throws IOException {
	   String separator = "/";
		String OS = System.getProperty("os.name").toLowerCase();
	    if (OS.indexOf("win") >= 0) {
	    	separator = "\\";
	    }
	   Set<String> filenames = f.getFilename();
	   int count = filenames.size();
	   log.info("Begin Validation Total file " + String.valueOf(count) );
	   log.info("Path " + path);
	   int l_count = 0;
	    for ( String fileName : filenames ) {
	    	File file = new File(path + separator + fileName);
	    	if ( !file.exists()) {
	    		log.error("File missing " + path + separator + fileName);
	    		return false;
	    	}else {
	    		l_count ++;
	    		log.info("File exist " + path + separator + fileName + String.valueOf(l_count));
	    	}
	    }
	    if ( l_count != count ) {
	    	log.error("Files total exist not tally with Batch request");
	    	return false;
	    }
	    log.info("Validation completed and successful.  Total Files " + String.valueOf(count) );
	    return true;
	}
   
   public boolean AllAttachmentExist( List<CharSequence> f, String path) 
			  throws IOException {
			   String separator = "/";
			   long attachment_total_size = 0;
				String OS = System.getProperty("os.name").toLowerCase();
			    if (OS.indexOf("win") >= 0) {
			    	separator = "\\";
			    }
			   
			   int count = f.size();
			   log.info("Begin Validation Attachment files " + String.valueOf(count) );
			   log.info("Path " + path);
			   int l_count = 0;
			   for (CharSequence single_f : f) {
			    	File file = new File(path + separator + single_f);
			    	if ( !file.exists()) {
			    		attachment_total_size += file.length();
			    		log.error("File missing " + path + separator + single_f);
			    		return false;
			    	}else {
			    		l_count ++;
			    		log.info("File exist " + path + separator + single_f + String.valueOf(l_count));
			    	}
			    }
			    if ( l_count != count ) {
			    	log.error("Attachments not tally with Batch request");
			    	return false;
			    }
			    log.info("Attachment Validation completed and successful.  Total Files  " + String.valueOf(count)  + " size = " + String.valueOf(attachment_total_size));
			    if ( attachment_total_size > 25000 ) {
			    	log.error("Attachments exceed 25KB");
			    	return false;	
			    }else {
			    	return true;
			    }
			}

   public boolean AllAttachmentBackup( List<CharSequence> f, String path) 
			  throws IOException {
			   String separator = "/";
				String OS = System.getProperty("os.name").toLowerCase();
			    if (OS.indexOf("win") >= 0) {
			    	separator = "\\";
			    }
			   
			   int count = f.size();
			   log.info("Backup Attachment" + String.valueOf(count) );
			   log.info("Path " + path);
			   int l_count = 0;
			   for (CharSequence single_f : f) {
				    
				    String fr = path + separator + "Upload" + separator + String.valueOf(single_f);
			    	File file = new File(fr);
			    	if ( !file.exists()) {
			    		log.error("File missing " + path + separator + String.valueOf(single_f));
			    		return false;
			    	}else {
			    		l_count ++;
			    		String target =path + "Backup" + separator + String.valueOf(single_f);
			    		log.info("Relocate " + fr + " to Backup folder " + target );
			    		Files.move(Paths.get(path + separator + "Upload" + separator + single_f), Paths.get(target ), StandardCopyOption.REPLACE_EXISTING);
			    	}
			    }
			    if ( l_count != count ) {
			    	log.error("Attachments not tally with Batch request");
			    	return false;
			    }
			    log.info("Attachment Backup completed and successful.  Total Files " + String.valueOf(count) );
			    return true;
			    
			}
	   
}