package tachyonis.space.batch;

import java.util.List;
import java.util.Set;

public class FileList {
	public static Set<String> filename;
	public static List<String> Status;
	public final Set<String> getFilename() {
		return filename;
	}

	public final void setFilename(Set<String> filename) {
		FileList.filename = filename;
	}

	public static final List<String> getStatus() {
		return Status;
	}

	public static final void setStatus(List<String> status) {
		Status = status;
	}
}
