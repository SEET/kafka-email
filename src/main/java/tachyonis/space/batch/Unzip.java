package tachyonis.space.batch;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import tachyonis.space.Config;

public class Unzip {
	static Logger log = LogManager.getLogger(Unzip.class);
	private static Config config;

    public Unzip(Config c){ 
    	Unzip.config = c;
    }  


	public void unzip(String location) throws IOException {
		String glob = "glob:**/*.gz";
		
		final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher(glob);
		Files.walkFileTree(Paths.get(location), new SimpleFileVisitor<Path>() {
			
			@Override
			//Loop files
			public FileVisitResult visitFile(Path fileZip,	BasicFileAttributes attrs) throws IOException {
				if (pathMatcher.matches(fileZip)) {
					
					byte[] buffer = new byte[1024];
					 try{
				  
						 String separator = "/";
				    		String OS = System.getProperty("os.name").toLowerCase();
						    if (OS.indexOf("win") >= 0) {
						    	separator = "\\";
						    }
						    String INPUT_FILE = fileZip.toString();
				    	 GZIPInputStream gzis = 
				    		new GZIPInputStream(new FileInputStream(INPUT_FILE));
				    	 String OUTPUT_FILE = INPUT_FILE.substring(0,fileZip.toString().indexOf(".gz"));
				    	 FileOutputStream out = new FileOutputStream(OUTPUT_FILE);
				        log.info("GUNZIP from " + fileZip.toString() + " to " + OUTPUT_FILE);
				        int len;
				        while ((len = gzis.read(buffer)) > 0) {
				        	out.write(buffer, 0, len);
				        }
				 
				        gzis.close();
				    	out.close();
				 
				    	System.out.println("Done");
				    	
				    }catch(IOException ex){
				       ex.printStackTrace();   
				    }
			    }
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc)
					throws IOException {
				return FileVisitResult.CONTINUE;
			}
		});//End Loop Folder
	}
	
	public static void unzip(String fileZip, String destPath) throws IOException {
	        File destDir = new File(destPath);
	        byte[] buffer = new byte[1024];
	        ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
	        ZipEntry zipEntry = zis.getNextEntry();
	        while (zipEntry != null) {
	            File newFile = newFile(destDir, zipEntry);
	            FileOutputStream fos = new FileOutputStream(newFile);
	            int len;
	            while ((len = zis.read(buffer)) > 0) {
	                fos.write(buffer, 0, len);
	            }
	            fos.close();
	            zipEntry = zis.getNextEntry();
	        }
	        zis.closeEntry();
	        zis.close();
	    }
	    
	
	private static void zip(String file, String gzipFile) {
        try {
            FileInputStream fis = new FileInputStream(file);
            FileOutputStream fos = new FileOutputStream(gzipFile);
            GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
            byte[] buffer = new byte[1024];
            int len;
            while((len=fis.read(buffer)) != -1){
                gzipOS.write(buffer, 0, len);
            }
            //close resources
            gzipOS.close();
            fos.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
	    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
	        File destFile = new File(destinationDir, zipEntry.getName());
	         
	        String destDirPath = destinationDir.getCanonicalPath();
	        String destFilePath = destFile.getCanonicalPath();
	         
	        if (!destFilePath.startsWith(destDirPath + File.separator)) {
	            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
	        }
	         
	        return destFile;
	    }

	    public void unTarGz( String location ) throws IOException {
	    	
	    	Path pathOutput = Paths.get(location);
	    	String glob = "glob:**/*.tar.gz";
			log.info("Begin unTarGz Path " + location);
			final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher(glob);
			Files.walkFileTree(Paths.get(location), new SimpleFileVisitor<Path>() {
				
				@Override
				//Loop files
				public FileVisitResult visitFile(Path pathInput,	BasicFileAttributes attrs) throws IOException {
					if (pathMatcher.matches(pathInput)) {
						log.info("Untar files " + pathInput.toString());
						TarArchiveInputStream tararchiveinputstream =
				            new TarArchiveInputStream(
				                new GzipCompressorInputStream(
				                    new BufferedInputStream( Files.newInputStream( pathInput ) ) ) );
			
				        ArchiveEntry archiveentry = null;
				        while( (archiveentry = tararchiveinputstream.getNextEntry()) != null ) {
				            Path pathEntryOutput = pathOutput.resolve( archiveentry.getName() );
				            if( archiveentry.isDirectory() ) {
				                if( !Files.exists( pathEntryOutput ) )
				                    Files.createDirectory( pathEntryOutput );
				            }
				            else
				                Files.copy( tararchiveinputstream, pathEntryOutput, StandardCopyOption.REPLACE_EXISTING );
				        }
			
				        tararchiveinputstream.close();
				        //Delete after untar
				        log.info("Delete Files " + pathInput.toString());
				        Files.delete(pathInput);
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc)
						throws IOException {
					return FileVisitResult.CONTINUE;
				}
			});//End Loop Folder	
	    }
}
