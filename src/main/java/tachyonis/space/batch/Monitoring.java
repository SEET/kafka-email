package tachyonis.space.batch;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import static java.nio.file.StandardCopyOption.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.CopyOption;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tachyonis.space.Config;
import org.apache.logging.log4j.LogManager;

public class Monitoring implements Runnable  {
	static Logger log = LogManager.getLogger(Monitoring.class);
	private static Config config;
	public Monitoring(Config c) {
		Monitoring.config = c;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(config.getGzip() == "true") {
			List<String> gzip_paths = config.gzip_path;
			for (String path : gzip_paths) {
				try {
					//TODO Unzip to configurations to FileList and folders of files
					Unzip u = new Unzip(config);
					//u.unzip(path);
					u.unTarGz(path);
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}
		}
		
		List<String> paths = config.monitoring_path;
		for (String path : paths) {
			try {
				File directory = new File(path);
			    if (! directory.exists()){
			    	log.error("Folder Monitor " + path.toString() + " does not exist");
			    }else {
			    	FilesfromFolder(path);
			    }
				//listFilesfromFolder(path);
				//match(path);
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		}
		
		try {
			Thread.sleep(config.getMonitoring_interval()*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	  }
	
	   //TODO implement Lock Read protections 2019 Dec 18
	   public static void LockRead(String fileName) 
  		  throws IOException {
  		    RandomAccessFile stream = new RandomAccessFile(fileName, "r");
  		    FileChannel channel = stream.getChannel();
  		 
  		    FileLock lock = null;
  		    try {
  		        lock = channel.tryLock();
  		    } catch (final OverlappingFileLockException e) {
  		        stream.close();
  		        channel.close();
  		    }
  		    lock.release();
  		    stream.close();
  		    channel.close();
  		}
	
	   
	   //Loading by matching file pattern json without FileList
	   public static void match(String location) throws IOException {
		String glob = "glob:**/*.json";
		final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher(
				glob);
		
		Files.walkFileTree(Paths.get(location), new SimpleFileVisitor<Path>() {
			final Loading l= new Loading(Monitoring.config);
			@Override
			public FileVisitResult visitFile(Path path,
					BasicFileAttributes attrs) throws IOException {
				if (pathMatcher.matches(path)) {
					Loading.jsontoavro(path.toString());					
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc)
					throws IOException {
				return FileVisitResult.CONTINUE;
			}
		});
	   }
	   public List<String> FilesfromFolder(String location ) throws IOException {
		    
			String glob = "glob:**/*.json";
			//String h = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
			//String y_h = String.valueOf(LocalDate.now().getDayOfYear()) + "_" + h;
			String y_h = "Backup";
			
			String OS = System.getProperty("os.name").toLowerCase();
			String separator = "/";
		    if (OS.indexOf("win") >= 0) {
		    	separator = "\\";
		    }
		    File directory = new File(location + separator + y_h);
		    if (! directory.exists()){directory.mkdir();}
			String config_location = location + separator + "Config" + separator;
			final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher(glob);
			Files.walkFileTree(Paths.get(config_location), new SimpleFileVisitor<Path>() {
				final Loading l= new Loading(Monitoring.config);
				@Override
				public FileVisitResult visitFile(Path path,
						BasicFileAttributes attrs) throws IOException {
					String separator = "/";
					String OS = System.getProperty("os.name").toLowerCase();
				    if (OS.indexOf("win") >= 0) {
				    	separator = "\\";
				    }
				    
				    
					if (pathMatcher.matches(path)) {
						try {
							String fullpath = location + "Process" ;
							String target = fullpath + separator + path.getFileName().toString();
							log.info("Copy from " + path.toString() + " to " + target );
							//Files.copy(Paths.get(path.toString()), Paths.get(target ), StandardCopyOption.REPLACE_EXISTING);
							Set<String> LoadByFileList = new HashSet<String>(); 
							LoadByFileList.add(path.toString());
							Loading.jsontoavroList(location,LoadByFileList);
							//Files.delete(Paths.get(target));
							//log.info("Delete from " + path.toString() + " to " + target );
							target = location + separator + "Backup" + separator + "Success" + separator + path.getFileName().toString();
							//Files.move(Paths.get(path.toString()), Paths.get(target ), StandardCopyOption.REPLACE_EXISTING);
						}catch(Exception e) {
							log.error(e.getMessage());
							log.error("Error loading one file ");
							//TODO Update the FileList to failures
						}	
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc)
						throws IOException {
					return FileVisitResult.CONTINUE;
				}
			});//End Loop Folder
			return null;
			
		}

    //Loading by matching file pattern json with FileList
	public List<String> listFilesfromFolder(String location ) throws IOException {
		String glob = "glob:**/req_*.json";
		String h = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		String y_h = String.valueOf(LocalDate.now().getDayOfYear()) + "_" + h;
		final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher(glob);
		
		Files.walkFileTree(Paths.get(location), new SimpleFileVisitor<Path>() {
			final Loading l= new Loading(Monitoring.config);
			@Override
			public FileVisitResult visitFile(Path path,
					BasicFileAttributes attrs) throws IOException {
				String separator = "/";
				String OS = System.getProperty("os.name").toLowerCase();
			    if (OS.indexOf("win") >= 0) {
			    	separator = "\\";
			    }
			    File directory = new File(location + separator + y_h);
			    if (! directory.exists()){directory.mkdir();}
			    File fail_directory = new File(location + separator + y_h + separator + "Fail");
			    if (! fail_directory.exists()){directory.mkdir();}
				if (pathMatcher.matches(path)) {
					ObjectMapper objectMapper = new ObjectMapper();
					//convert Files to 
					log.info("Processing Batch File " + path.toString());
					try {
						FileList jsonFileList = objectMapper.readValue(path.toFile(), FileList.class);
						String fullpath = path.toString().substring(0,path.toString().indexOf(".json"));
						directory = new File(fullpath);
						if (! directory.exists()){
					        log.debug("Missing folder " + fullpath + " containing the Batch Files");
					    }else {
					    	Validate v = new Validate();
							if ( v.AllfilesExist(jsonFileList, fullpath)) {
								Set<String> LoadByFileList = jsonFileList.getFilename();
								//Full List of filename
							    try {
							    	//2019 Dec 24 Not yet implmented mechanism to feedback KafKa producer metadata because of async function of producer send
									Loading.jsontoavroList(fullpath,LoadByFileList);
									//String target_folder = location + y_h + separator + path.getFileName().toString().substring(0,path.getFileName().toString().indexOf(".json")) ;
									String target = location + y_h + separator + path.getFileName().toString() +".DONE";
									Files.move(Paths.get(path.toString()), Paths.get(target ), StandardCopyOption.REPLACE_EXISTING);
								}catch(Exception e) {
									log.error(e.getMessage());
									log.error("Error loading one file ");
									//TODO Update the FileList to failures
								}							
				    		}else {
				    			log.error("Validation Failed.  Not all files exists");
				    		}
						}
					}catch ( JsonParseException e) {
						String target = fail_directory.toString() + separator + path.getFileName().toString() + ".JsonParseException";
						Files.move(Paths.get(path.toString()), Paths.get(target), REPLACE_EXISTING);
					}catch ( JsonMappingException e) {
						String target = fail_directory.toString() + separator + path.getFileName().toString() + ".JsonMappingException";
						Files.move(Paths.get(path.toString()), Paths.get(target), REPLACE_EXISTING);
					}catch( IOException e) {
						String target = fail_directory.toString() + separator + path.getFileName().toString() + ".IOException";
						Files.move(Paths.get(path.toString()), Paths.get(target), REPLACE_EXISTING);
					}
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc)
					throws IOException {
				return FileVisitResult.CONTINUE;
			}
		});//End Loop Folder
		return null;
	}
}