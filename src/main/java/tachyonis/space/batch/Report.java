package tachyonis.space.batch;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tachyonis.space.AvroEml;
import tachyonis.space.Config;
import tachyonis.space.JsonEml;
import tachyonis.space.kafka.MailConsumer;
import tachyonis.space.kafka.MailProducer;

public class Report implements Runnable {
	static Logger log = LogManager.getLogger(Report.class);
	private static Config config;

    public Report(Config c){ 
    	Report.config = c;
    }  

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
	private static void report(String ReportFile, String fileName, String status) {
		new Thread() {
		@Override
	    public void run() {
	    		try {
					log.info("Batch loading Thread");
				} catch (Exception e) {
					log.error(e.getMessage());
				}
	    	}
		}.start();
		log.info("Producer End Thread");
	}
	
    public static void movefile(String fileName) {
    	ObjectMapper objectMapper = new ObjectMapper();
    	
    	File file = new File(fileName);
    	AvroEml eml = new AvroEml();
    	try {
			JsonEml json = objectMapper.readValue(file, JsonEml.class);
			eml.setId(UUID.randomUUID().toString());
			eml.setAttachment(json.getAttachment());
			eml.setBody(json.getBody());
			eml.setReceiver(json.getReceiver());
			eml.setSendDate(json.getSendDate());
			eml.setSender(json.getSender());
			eml.setSubject(json.getSender());
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
