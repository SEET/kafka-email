package tachyonis.space;

//import static org.junit.Assert.assertEquals;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import tachyonis.space.Config;
import tachyonis.space.batch.Monitoring;
import tachyonis.space.email.EmailService;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import tachyonis.space.kafka.MailConsumer;

@SpringBootApplication
public class admpvse implements CommandLineRunner{
	private static final Logger log = LogManager.getLogger(admpvse.class);
	@Autowired
    private Config c;
	
	public admpvse() {
    }
	
	public static void main(String[] args) {
		singleton s = new singleton();
		if ( s.isSingleton("ADMPVSE")){
			SpringApplication.run(admpvse.class, args);
		}else{
			// Need Java JDK 13 
			// s.isJavaProcessAlive();
			log.error("Existing Process is running");
			
		};
	}
	
	public void run(String... args) throws Exception {
		//FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("appContext.xml");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
		EmailService mailer = (EmailService) context.getBean("emailService");
		context.close();
		/*
		Monitoring b = new Monitoring(c);
		MailConsumer x = new MailConsumer(mailer,c);
		MailConsumer y = new MailConsumer(mailer,c);
		MailConsumer z = new MailConsumer(mailer,c);
		*/
		/*TODO: To change from predefined workers Thread to dynamic Tread Pool to always maintained fixed number of Threads  
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        executor.setMaximumPoolSize(c.getMax_consumer_intance_per_group());
        for( int i =1;i<=c.getMax_consumer_intance_per_group();i++) {
        	executor.submit(() -> {
	    		try {
					m.run(a);
					assertEquals(c.getMax_consumer_intance_per_group(), executor.getPoolSize());
		            assertEquals(0, executor.getQueue().size());
				} catch (Exception e) {
					log.error(e.getMessage());
				}
    		    return null;
    	});*/
		

		final ScheduledExecutorService scheduler =  Executors.newScheduledThreadPool(c.getMax_consumer_intance_per_group());
		final ScheduledExecutorService batch =  Executors.newScheduledThreadPool(1);
		/*
		final Runnable mailconsumer1 = new Runnable() {
		       public void run() { 
		    	   log.info("Thread X restarted");
		    	   x.stop();
		    	   x.run(); 
		       }
		};
		final Runnable mailconsumer2 = new Runnable() {
			   public void run() {
				   log.info("Thread Y restarted");
				   y.stop();
		    	   y.run(); 
		       }
		};

		final Runnable mailconsumer3 = new Runnable() {
		       public void run() { 
		    	   log.info("Thread Z restarted");
		    	   z.stop();
		    	   z.run(); 
		       }
		};
		
		final ScheduledFuture<?> shutHandle1 = scheduler.scheduleAtFixedRate(mailconsumer1, 33, 30000, TimeUnit.MILLISECONDS);
		final ScheduledFuture<?> shutHandle2 = scheduler.scheduleAtFixedRate(mailconsumer2, 66, 60000, TimeUnit.MILLISECONDS);
		final ScheduledFuture<?> shutHandle3 = scheduler.scheduleAtFixedRate(mailconsumer3, 99, 90000, TimeUnit.MILLISECONDS);
		*/
		while (true) {
			final ScheduledFuture<?> shutHandle4 = scheduler.scheduleAtFixedRate(new MailConsumer(mailer,c), 99, 90000, TimeUnit.MILLISECONDS);
			final ScheduledFuture<?> shutHandle5 = batch.scheduleAtFixedRate(new Monitoring(c), 3, 300, TimeUnit.SECONDS);
			Thread.sleep(7000);
		}
		
		/*scheduler.schedule(new Runnable() {
			public void run() { shutHandle1.cancel(true); }
		}, 3 , TimeUnit.SECONDS);
		scheduler.schedule(new Runnable() {
			public void run() { shutHandle2.cancel(true); }
		}, 6 , TimeUnit.SECONDS);
		scheduler.schedule(new Runnable() {
			public void run() { shutHandle3.cancel(true); }
		}, 9 , TimeUnit.SECONDS);
        */
		//Integrate Test to be used only for testing phase
		/*MailProducer p = new MailProducer();
		new Thread() {
		@Override
	    public void run() {
	    		try {
					p.run(c);
					log.info("Producer Start new Thread");
				} catch (Exception e) {
					log.error(e.getMessage());
				}
	    	}
		}.start();
		log.info("Producer End Thread");
		*/
		/*
    	for( int i =1;i<=c.getMax_consumer_intance_per_group();i++) {
			new Thread() {
			@Override
		    public void run() {
		    		try {
						m.run(c);
					} catch (Exception e) {
						log.error(e.getMessage());
					}
		    	}
			}.start();
        }
    	*/
	}
 }