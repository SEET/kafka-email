package tachyonis.space;

public class JsonEml {
	  public java.lang.CharSequence sender;
	  public java.lang.CharSequence receiver;
	  public java.lang.CharSequence bcc;
	  public java.lang.CharSequence cc;
	  public java.lang.CharSequence subject;
	  public java.lang.CharSequence sendDate;
	  public java.lang.String body;
	  public java.util.List<java.lang.CharSequence> attachment;
	  public java.lang.String path;

	public final java.lang.CharSequence getSender() {
		return sender;
	}
	public final void setSender(java.lang.CharSequence sender) {
		this.sender = sender;
	}
	public final java.lang.CharSequence getReceiver() {
		return receiver;
	}
	public final void setReceiver(java.lang.CharSequence receiver) {
		this.receiver = receiver;
	}
	public final java.lang.CharSequence getSubject() {
		return subject;
	}
	public final void setSubject(java.lang.CharSequence subject) {
		this.subject = subject;
	}
	public final java.lang.CharSequence getSendDate() {
		return sendDate;
	}
	public final void setSendDate(java.lang.CharSequence sendDate) {
		this.sendDate = sendDate;
	}
	public final java.lang.String getBody() {
		return body;
	}
	public final void setBody(java.lang.String body) {
		this.body = body;
	}
	public final java.util.List<java.lang.CharSequence> getAttachment() {
		return attachment;
	}
	public final void setAttachment(java.util.List<java.lang.CharSequence> attachment) {
		this.attachment = attachment;
	}
	public final java.lang.String getPath() {
		return path;
	}
	public final void setPath(java.lang.String path) {
		this.path = path;
	}
	public final java.lang.CharSequence getBcc() {
		return bcc;
	}
	public final void setBcc(java.lang.CharSequence bcc) {
		this.bcc = bcc;
	}
	public final java.lang.CharSequence getCc() {
		return cc;
	}
	public final void setCc(java.lang.CharSequence cc) {
		this.cc = cc;
	}
}
