//This Class generated from application.yml

package tachyonis.space;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class Config {
  
    private String name;
    private String environment;
    private String topic;
    private String bootstrap_servers_config;
    private String context;
    private int max_consumer_intance_per_group;
    private String schema_registry_url_config;
    public Boolean email_test;
    public int monitoring_interval;
    public List<String> monitoring_path;
    public String gzip;
    public List<String> gzip_path;

	public final List<String> getGzip_path() {
		return gzip_path;
	}
	public final void setGzip_path(List<String> gzip_path) {
		this.gzip_path = gzip_path;
	}
	public final Boolean getEmail_test() {
		return email_test;
	}
	public final void setEmail_test(Boolean email_test) {
		this.email_test = email_test;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}
	/**
	 * @param environment the environment to set
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	/**
	 * @return the topic
	 */
	public String getTopic() {
		return topic;
	}
	/**
	 * @param topic the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}
	/**
	 * @return the bootstrap_servers_config
	 */
	public String getBootstrap_servers_config() {
		return bootstrap_servers_config;
	}
	/**
	 * @param bootstrap_servers_config the bootstrap_servers_config to set
	 */
	public void setBootstrap_servers_config(String bootstrap_servers_config) {
		this.bootstrap_servers_config = bootstrap_servers_config;
	}
	/**
	 * @return the context
	 */
	public String getContext() {
		return context;
	}
	/**
	 * @param context the context to set
	 */
	public void setContext(String context) {
		this.context = context;
	}
	/**
	 * @return the max_consumer_intance_per_group
	 */
	public int getMax_consumer_intance_per_group() {
		return max_consumer_intance_per_group;
	}
	/**
	 * @param max_consumer_intance_per_group the max_consumer_intance_per_group to set
	 */
	public void setMax_consumer_intance_per_group(int max_consumer_intance_per_group) {
		this.max_consumer_intance_per_group = max_consumer_intance_per_group;
	}
	/**
	 * @return the schema_registry_url_config
	 */
	public String getSchema_registry_url_config() {
		return schema_registry_url_config;
	}
	/**
	 * @param schema_registry_url_config the schema_registry_url_config to set
	 */
	public void setSchema_registry_url_config(String schema_registry_url_config) {
		this.schema_registry_url_config = schema_registry_url_config;
	}
	public final List<String> getMonitoring_path() {
		return monitoring_path;
	}
	public final void setMonitoring_path(List<String> monitoring_path) {
		this.monitoring_path = monitoring_path;
	}
	public final int getMonitoring_interval() {
		return monitoring_interval;
	}
	public final void setMonitoring_interval(int monitoring_interval) {
		this.monitoring_interval = monitoring_interval;
	}
	public final String getGzip() {
		return gzip;
	}
	public final void setGzip(String gzip) {
		this.gzip = gzip;
	}
    
  


}
