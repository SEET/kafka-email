package tachyonis.space.email;

import java.io.File;
import java.util.List;
import java.lang.CharSequence;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager; 
@Service("emailService")
public class EmailService 
{
	private static final Logger log = LogManager.getLogger(EmailService.class);
	    
	@Autowired
    private JavaMailSender mailSender;
	
	@Autowired
    private JavaMailSender mailSenderTest;
      
    @Autowired
    private SimpleMailMessage preConfiguredMessage;
    
    /**
     * Author Seet Hing Long
     * mailSender and preconfiguredMessage are configured in maven src/main/java/appContext.xml Bean
     * 
     * */
    
    public void sendMail(String to, String subject, String message) throws MailException
    {
    	// preConfiguredMessage configured in appContext.xml
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setTo(to);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
       	mailSender.send(mailMessage);
    }
    
    //AVRO data extraction to charSequence has problem to use String in the arguments
    public void sendMailchar(CharSequence to, CharSequence subject, CharSequence message) throws MailException
    {
    	// preConfiguredMessage configured in appContext.xml
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setTo(to.toString());
        mailMessage.setSubject(subject.toString());
        mailMessage.setText(message.toString());
       	mailSender.send(mailMessage);
    }
    /**
     * This method will send a pre-configured message
     * */
    public void sendPreConfiguredMail() throws MailException
    {
    	// preConfiguredMessage configured in appContext.xml
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setText("Thursday, November 14, 2019\r\n" + 
        		"Financial Reset\r\n" + 
        		"Our Silver Trigger activation has reached about 80% of the critical mass. Although we have not reached our goal, our achievement was significant, as it will still ease the planetary path towards the financial Reset.\r\n" + 
        		"\r\n" + 
        		"Tens of thousands of Lightworkers were buying silver and this will become the seed for the manifestation of their future abundance, as I will describe in one of the coming blog posts in detail.\r\n" + 
        		"\r\n" + 
        		"In early morning hours in Europe, shortly after midnight on November 11th, a certain “family” has bought over 100 tonnes of silver for immediate delivery, on Sydney exchange. This has skyrocketed the silver price, as it can be seen on the following screenshot from Kitco website, one of the main referral sites for the price of silver");
        mailSenderTest.send(mailMessage);
    }
    
    public void sendMailWithAttachment(CharSequence fr, CharSequence to, CharSequence bcc, CharSequence cc, CharSequence subject, CharSequence body, List<CharSequence> fileToAttach, CharSequence path) throws MailException
    {
    	MimeMessagePreparator preparator = new MimeMessagePreparator() 
        {
            public void prepare(MimeMessage mimeMessage) throws Exception 
            {   
            	if(fr != null && fr.length() > 0) {
            		mimeMessage.setFrom(new InternetAddress(fr.toString()));
            	}else {
            		log.error("Missing Sender");
            	}
            	if ( to != null && to.length() > 0) {
            		mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to.toString()));
            	}else {
            		log.error("Missing Recipient");
            	}
            	if ( bcc != null && bcc.length() > 0 ) {
            		mimeMessage.setRecipient(Message.RecipientType.BCC, new InternetAddress(bcc.toString()));
            	}else {
            		log.info("Optional Bcc Empty");
            	}
            	if ( cc != null && cc.length() > 0 ) {
            		mimeMessage.setRecipient(Message.RecipientType.CC, new InternetAddress(cc.toString()));
            	}else {
            		log.info("Optional Cc Empty");
            	}
            	if ( subject != null && subject.length() > 0 ) {
            		mimeMessage.setSubject(subject.toString());
            	}else {
            		log.info("Empty Subject");
            	}
                
                Multipart multiPart = new MimeMultipart();
                
                long size = 0;
                
                log.info("Preparing email body");
        		
                if ( body.toString().length() > 0) {
                	if (body.toString().contains("DOCTYPE html")) {
                		//String htmlString = body.toString().replaceAll("\"", "\\\"");
                		//htmlString = htmlString.replaceAll("/", "\\/");
                		//byte[] bytes = htmlString.replaceAll("\\r\\n", "").getBytes(); 
                		//byte[] bytes = body.toString().getBytes("UTF-8");
                		//DataSource dataSourceHtml= new ByteArrayDataSource(bytes, "text/html;charset=UTF-8");
                		//DataSource dataSourceHtml= new ByteArrayDataSource(htmlemail(), "text/html");
                		MimeBodyPart htmlPart = new MimeBodyPart();
                		//htmlPart.setDataHandler(new DataHandler(dataSourceHtml));
                		//htmlPart.setContent(htmlemail(), "text/html;charset=UTF-8");
                		htmlPart.setContent(body.toString(), "text/html;charset=UTF-8");
                		multiPart.addBodyPart(htmlPart);
                		log.info("Body is HTML");
                	}else {
                		MimeBodyPart textPart = new MimeBodyPart();
                        textPart.setText(body.toString(), "utf-8");
                		multiPart.addBodyPart(textPart);
                	}
                	size += body.toString().length();
                }else {
                	//mimeMessage.setText(" ","UTF-8","html");
                	log.warn("Empty Body");
                }
                
                if ( fileToAttach != null && fileToAttach.size() > 0 ) {
	                for (CharSequence singlefile : fileToAttach) {
	                	String fullpathfileToAttach = path.toString();
	                	if ( path.toString().charAt(path.length()-1) == '/' ){
	                		fullpathfileToAttach = fullpathfileToAttach + singlefile.toString();
	                	}else {
	                		fullpathfileToAttach = fullpathfileToAttach + "/" + singlefile.toString();
	                	}
	                    File file = new File(fullpathfileToAttach);
	                    log.info("Attachment Path : " + fullpathfileToAttach);
	                    log.info("Filename:" + singlefile); 
	                    if ( singlefile.toString().length() > 0 ) {
		                	if (file.exists() ) {
		                		size += file.length();
		                		log.info("@@ Attach File " + file.toString());
		                		MimeBodyPart attachementPart = new MimeBodyPart();
		                        DataSource source = new FileDataSource(file) {
		                        	@Override
		                        	public String getContentType()
		                        	{
		                        		return "application/octet-stream";
		                        	}
		                        };
		                        attachementPart.setDataHandler(new DataHandler(source));
		                        attachementPart.setFileName(singlefile.toString());
		                        multiPart.addBodyPart(attachementPart);
		                	}
		                }
	                }
                }else {
                	log.info("No attachment");
                }
                mimeMessage.setContent(multiPart);
                log.info("@@@ Total file size = " + String.valueOf(size));
            }
        };
        mailSender.send(preparator);
        
    }
    
    public void sendMailWithInlineResources(String to, String subject, String fileToAttach) throws MailException
    {
        MimeMessagePreparator preparator = new MimeMessagePreparator() 
        {
            public void prepare(MimeMessage mimeMessage) throws Exception 
            {
                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
                mimeMessage.setFrom(new InternetAddress("admin@tachyonis.space"));
                mimeMessage.setSubject(subject);
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
                helper.setText("<html><body><img src='cid:identifier1234'></body></html>", true);                
                FileSystemResource res = new FileSystemResource(new File(fileToAttach));
                helper.addInline("identifier1234", res);
            }
        };
        mailSender.send(preparator);
    }
    
    private String htmlemail() {
    	String body = "<!DOCTYPE html><html lang=3Den><head><h1>TESTING HTML</h1></body></html>";
    	return body;
    }
}