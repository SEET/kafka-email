package tachyonis.space;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class singleton {
	private static final Logger log = LogManager.getLogger(singleton.class);
	
	
	
	/*Below need Oracle JDK 13 Seet Hing Long 2019 Dec 
	boolean isJavaProcessAlive() {
		System.out.println(processDetails(ProcessHandle.current()));
		//ProcessHandle.allProcesses()
		//.forEach(process -> System.out.println(processDetails(process)));
		return false;
	}
	
	private static String processDetails(ProcessHandle process) {
	    return String.format("%8d %8s %10s %26s %-40s",
	            process.pid(),
	            text(process.parent().map(ProcessHandle::pid)),
	            text(process.info().user()),
	            text(process.info().command()),
	            text(process.info().arguments()));
	}
    
	private static String text(Optional<?> optional) {
	    return optional.map(Object::toString).orElse("-");
	}*/
	
	boolean isStillAllive(String Str) {
	    String OS = System.getProperty("os.name").toLowerCase();
	    String command = null;
	    if (OS.indexOf("win") >= 0) {
	    	
	        log.debug("Check alive Windows mode. Pid: [{}]", Str);
	        command = "cmd /c wmic process get Commandline,Processid";
	        return isProcessIdRunning(Str, command);
	    } else if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0) {
	        log.debug("Check alive Linux/Unix mode. Pid: [{}]", Str);
	        command = "ps -eaf|grep -v grep|grep " + Str;
	        return isProcessIdRunning(Str, command);
	    } 
	    log.debug("Default Check alive for Pid: [{}] is false", Str);
	    return false;
	}
	boolean isSingleton(String Str) {
	    String OS = System.getProperty("os.name").toLowerCase();
	    String command = null;
	    if (OS.indexOf("win") >= 0) {
	    	
	        log.debug("Check alive Windows mode. Pid: [{}]", Str);
	        command = "cmd /c wmic process get Commandline,Processid";
	        return isProcessIdRunningSingleton(Str, command);
	    } else if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0) {
	        log.debug("Check alive Linux/Unix mode. Pid: [{}]", Str);
	        command = "ps -eaf";
	        return isProcessIdRunningSingleton(Str, command);
	    } 
	    log.debug("Default Check alive for Pid: [{}] is false", Str);
	    return true;
	}


	private boolean isProcessIdRunning(String pid, String command) {
	    log.debug("Command [{}]",command );
	    try {
	        Runtime rt = Runtime.getRuntime();
	        Process pr = rt.exec(command);

	        InputStreamReader isReader = new InputStreamReader(pr.getInputStream());
	        BufferedReader bReader = new BufferedReader(isReader);
	        String strLine = null;
	        int count =0;
	        while ((strLine= bReader.readLine()) != null) {
	            if (strLine.contains(pid)) {
	            	count++;
	            	log.debug(strLine);
	                if (count > 1) {
	                	return true;
	                }
	            }
	        }
	        return false;
	        
	    } catch (Exception ex) {
	        log.warn("Got exception using system command [{}].", command, ex);
	        return true;
	    }
	}
	
	private boolean isProcessIdRunningSingleton(String pid, String command) {
	    log.debug("Command [{}]",command );
	    try {
	        Runtime rt = Runtime.getRuntime();
	        Process pr = rt.exec(command);

	        InputStreamReader isReader = new InputStreamReader(pr.getInputStream());
	        BufferedReader bReader = new BufferedReader(isReader);
	        String strLine = null;
	        int count = 0;
	        while ((strLine= bReader.readLine()) != null) {
	            if (strLine.contains(pid)) {
	            	count++;
	            	log.debug(strLine);
	            }
	        }
	        if ( count==1) {
	        	return true;
	        }else {
	        	log.debug("ProcessIdRunningSingleton @ count = " + String.valueOf(count));
	        	return false;
	        }
	        
	    } catch (Exception ex) {
	        log.warn("Got exception using system command [{}].", command, ex);
	        return true;
	    }
	}
	
	public static void showAllProcessesRunningOnWindows(){
        try {
            Runtime runtime = Runtime.getRuntime();
            String cmds[] = {"cmd", "/c", "tasklist"};
            Process proc = runtime.exec(cmds);
            InputStream inputstream = proc.getInputStream();
            InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
            BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
            String line;
            while ((line = bufferedreader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Cannot query the tasklist for some reason.");
        }
    }

}
