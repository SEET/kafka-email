package tachyonis.space;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailException;
import tachyonis.space.email.EmailService;

@SpringBootApplication
public class HtmlEmail {
	
	@Autowired
    private Config c;
	
	public static void main(String[] args) throws Exception 
    {
		String body = "<!DOCTYPE html><html lang=3Den><head><meta name=3Dformat-detection content=\r\n" + 
				"=3Demail=3Dno /><meta name=3Dformat-detection content=3Ddate=3Dno /><style =\r\n" + 
				"nonce=3DXF3fGofIIh8nC12K8WBmOA>.awl a {color: #FFFFFF; text-decoration: non=\r\n" + 
				"e;} .abml a {color: #000000; font-family: Roboto-Medium,Helvetica,Arial,san=\r\n" + 
				"s-serif; font-weight: bold; text-decoration: none;} .adgl a {color: rgba(0,=\r\n" + 
				" 0, 0, 0.87); text-decoration: none;} .afal a {color: #b0b0b0; text-decorat=\r\n" + 
				"ion: none;} @media screen and (min-width: 600px) {.v2sp {padding: 6px 30px =\r\n" + 
				"0px;} .v2rsp {padding: 0px 10px;}} @media screen and (min-width: 600px) {.m=\r\n" + 
				"dv2rw {padding: 40px 40px;}} </style><link href=3D//fonts.googleapis.com/cs=\r\n" + 
				"s?family=3DGoogle+Sans rel=3Dstylesheet type=3Dtext/css /></head><body styl=\r\n" + 
				"e=3D\"margin: 0; padding: 0;\" bgcolor=3D#FFFFFF><img src=3Dhttps://notificat=\r\n" + 
				"ions.googleapis.com/email/t/AFG8qyUO0s6iq9IgbwxoQRt9Oa4vl3DncsOj5Dn4OlPNZOX=\r\n" + 
				"3FPFVcZGLLY28JCmDp1xbul1uLgS2_6PAiD90QaBBFgpcRDY6XemdhGiRqfRZIEU1u7h_iGXhNF=\r\n" + 
				"WtDi1JJ_1dCIRVgjByTgfxEtheVMUoP_F2LzO8jd8K_c3YPxcWyPWEJKhulwqGnSFhT0n8Ry3EH=\r\n" + 
				"d7JvWXGLdfU2IGLYj7_o_AoY1JWEdRDr9MQ-sKO-mrH6MbR8O02Iy6G2WZt930qTik/a.gif wi=\r\n" + 
				"dth=3D1 height=3D1><table width=3D100% height=3D100% style=3D\"min-width: 34=\r\n" + 
				"8px;\" border=3D0 cellspacing=3D0 cellpadding=3D0 lang=3Den><tr height=3D32 =\r\n" + 
				"style=3D\"height: 32px;\"><td></td></tr><tr align=3Dcenter><td><div itemscope=\r\n" + 
				" itemtype=3D//schema.org/EmailMessage><div itemprop=3Daction itemscope item=\r\n" + 
				"type=3D//schema.org/ViewAction><link itemprop=3Durl href=3Dhttps://accounts=\r\n" + 
				".google.com/AccountChooser?Email=3Dseethinglong@gmail.com&amp;continue=3Dht=\r\n" + 
				"tps://myaccount.google.com/alert/nt/1573114026000?rfn%3D31%26rfnc%3D1%26eid=\r\n" + 
				"%3D6173919184306135118%26et%3D0%26anexp%3Dhsc-control_a--staticndn-fa /><me=\r\n" + 
				"ta itemprop=3Dname content=3D\"Review Activity\" /></div></div><table border=\r\n" + 
				"=3D0 cellspacing=3D0 cellpadding=3D0 style=3D\"padding-bottom: 20px; max-wid=\r\n" + 
				"th: 516px; min-width: 220px;\"><tr><td width=3D8 style=3D\"width: 8px;\"></td>=\r\n" + 
				"<td><div style=3D\"border-style: solid; border-width: thin; border-color:#da=\r\n" + 
				"dce0; border-radius: 8px; padding: 40px 20px 36px 20px;\" align=3Dcenter cla=\r\n" + 
				"ss=3Dmdv2rw><img src=3Dhttps://www.gstatic.com/images/branding/googlelogo/1=\r\n" + 
				"x/googlelogo_color_74x24dp.png srcset=3D\"https://www.gstatic.com/images/bra=\r\n" + 
				"nding/googlelogo/2x/googlelogo_color_74x24dp.png 2x\" width=3D74 height=3D24=\r\n" + 
				" aria-hidden=3Dtrue style=3D\"margin-bottom: 16px;\"><div style=3D\"font-famil=\r\n" + 
				"y: &#39;Google Sans&#39;,Roboto,RobotoDraft,Helvetica,Arial,sans-serif;bord=\r\n" + 
				"er-bottom: thin solid #dadce0; color: rgba(0,0,0,0.87); line-height: 32px; =\r\n" + 
				"padding-bottom: 24px;text-align: center; word-break: break-word;\"><div styl=\r\n" + 
				"e=3D\"font-size: 24px;\">New device signed in&nbsp;to</div><table align=3Dcen=\r\n" + 
				"ter style=3Dmargin-top:8px;><tr style=3D\"line-height: normal;\"><td align=3D=\r\n" + 
				"right style=3Dpadding-right:8px;><img width=3D20 height=3D20 style=3D\"width=\r\n" + 
				": 20px; height: 20px; vertical-align: sub; border-radius: 50%;;\" src=3Dhttp=\r\n" + 
				"s://lh3.googleusercontent.com/a-/AAuE7mBmlA6TeI3rxKGONrSNKwBu-kOhMqKi9z9kEc=\r\n" + 
				"xTJkw=3Ds96></td><td><a style=3D\"font-family: &#39;Google Sans&#39;,Roboto,=\r\n" + 
				"RobotoDraft,Helvetica,Arial,sans-serif;color: rgba(0,0,0,0.87); font-size: =\r\n" + 
				"14px; line-height: 20px;\">seethinglong@gmail.com</a></td></tr></table></div=\r\n" + 
				"><div style=3D\"font-family: Roboto-Regular,Helvetica,Arial,sans-serif; font=\r\n" + 
				"-size: 14px; color: rgba(0,0,0,0.87); line-height: 20px;padding-top: 20px; =\r\n" + 
				"text-align: center;\">Your Google Account was just signed in to from a new W=\r\n" + 
				"indows device. You're getting this email to make sure it was you.<div style=\r\n" + 
				"=3D\"padding-top: 32px; text-align: center;\"><a href=3Dhttps://accounts.goog=\r\n" + 
				"le.com/AccountChooser?Email=3Dseethinglong@gmail.com&amp;continue=3Dhttps:/=\r\n" + 
				"/myaccount.google.com/alert/nt/1573114026000?rfn%3D31%26rfnc%3D1%26eid%3D61=\r\n" + 
				"73919184306135118%26et%3D0%26anexp%3Dhsc-control_a--staticndn-fa target=3D_=\r\n" + 
				"blank style=3D\"font-family: &#39;Google Sans&#39;,Roboto,RobotoDraft,Helvet=\r\n" + 
				"ica,Arial,sans-serif; line-height: 16px; color: #ffffff; font-weight: 400; =\r\n" + 
				"text-decoration: none;font-size: 14px;display:inline-block;padding: 10px 24=\r\n" + 
				"px;background-color: #4184F3; border-radius: 5px; min-width: 90px;\">Check a=\r\n" + 
				"ctivity</a></div><br><div style=3D\"font-size: 12px; line-height: 16px; colo=\r\n" + 
				"r: rgba(0,0,0,0.54); letter-spacing: 0.3px\">You can also go directly to <a =\r\n" + 
				"style=3D\"color: rgba(0, 0, 0, 0.87);\">myaccount.google.com/alert</a></div><=\r\n" + 
				"/div></div><div style=3D\"text-align: left;\"><div style=3D\"font-family: Robo=\r\n" + 
				"to-Regular,Helvetica,Arial,sans-serif;color: rgba(0,0,0,0.54); font-size: 1=\r\n" + 
				"1px; line-height: 18px; padding-top: 12px; text-align: center;\"><div>You re=\r\n" + 
				"ceived this email to let you know about important changes to your Google Ac=\r\n" + 
				"count and services.</div><div style=3D\"direction: ltr;\">&copy; 2019 Google =\r\n" + 
				"LLC, <a class=3Dafal style=3D\"font-family: Roboto-Regular,Helvetica,Arial,s=\r\n" + 
				"ans-serif;color: rgba(0,0,0,0.54); font-size: 11px; line-height: 18px; padd=\r\n" + 
				"ing-top: 12px; text-align: center;\">1600 Amphitheatre Parkway, Mountain Vie=\r\n" + 
				"w, CA 94043, USA</a></div></div></div></td><td width=3D8 style=3D\"width: 8p=\r\n" + 
				"x;\"></td></tr></table></td></tr><tr height=3D32 style=3D\"height: 32px;\"><td=\r\n" + 
				"></td></tr></table></body></html>";
        //Create the application context
		try {
			
			CharSequence from = new StringBuffer("hing-long.seet@yahoo.com");
			CharSequence bcc = new StringBuffer("");
			CharSequence cc = new StringBuffer("");
			CharSequence receiver = new StringBuffer("seethinglong@gmail.com");
			CharSequence subject = new StringBuffer("HTML email");
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
			EmailService mailer = (EmailService) context.getBean("emailService");
			mailer = new EmailService();
	    	mailer.sendMailWithAttachment(
	    			from,
	    			receiver, 
	    			bcc,
	    			cc,
	    			subject,
	    			body, 
	    			null,
	    			null);
	    	// The records must be one only set by consumer MAX_POLL_RECORDS_CONFIG

	    	//mailer.sendMailchar(value.getReceiver(),value.getSubject(),value.getBody());
	    	}catch(MailException e){
	    		
	    	}

    }
	public static void sendEmail(String name, String fromEmail, String subject, String message) throws AddressException, MessagingException, UnsupportedEncodingException, SendFailedException
	{
	    //Set Mail properties
	    java.util.Properties props = System.getProperties();
	    props.setProperty("mail.smtp.starttls.enable", "true");
	    props.setProperty("mail.smtp.host", "smtp.gmail.com");
	    props.setProperty("mail.smtp.socketFactory.port", "465");
	    props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    props.setProperty("mail.smtp.auth", "true");
	    props.setProperty("mail.smtp.port", "465");
	    Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
	        @Override
	        protected PasswordAuthentication getPasswordAuthentication() {
	            return new PasswordAuthentication("hing-long.seet@yahoo.com", "eklgdbyjgrumgbgm");
	        }
	    });

	    //Create the email with variable input
	    MimeMessage mimeMessage = new MimeMessage(session);
	    mimeMessage.setHeader("Content-Type", "text/plain; charset=UTF-8");
	    mimeMessage.setFrom(new InternetAddress(fromEmail, name));
	    mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("my_email"));
	    mimeMessage.setSubject(subject, "utf-8");
	    mimeMessage.setContent(message, "text/plain");

	    //Send the email
	    Transport.send(mimeMessage);
	}

}
