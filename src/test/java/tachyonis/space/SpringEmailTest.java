package tachyonis.space;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import tachyonis.space.email.EmailService;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class SpringEmailTest 
{
	static Logger log = LogManager.getLogger(SpringEmailTest.class);
    @SuppressWarnings("resource")
	public static void main(String[] args) 
    {
        //Create the application context
        ApplicationContext context = new FileSystemXmlApplicationContext
                    (args[0]);
          
        //Get the mailer instance
        EmailService mailer = (EmailService) context.getBean("emailService");
          
        //Send a composed mail
        //mailer.sendMail("seethinglong@gmail.com", "JavaMail from SUNTEC", "PCCW Solutions");
  
        //Send a pre-configured mail
        mailer.sendPreConfiguredMail();
//Get the mailer instance
        
        //Ensure Email Authentication is done before polling
        //log.info("Email Service Testing before started Polling " );

	        try {
	        	// Remove comment below if you need Mail Test before start KafKa Connections
	        	// It ensure the email name and password is fixed and it cannot be dynamically changed
	        	// Uncomment below to include test before polling the Thread
	        	mailer.sendPreConfiguredMail();
	        }catch(org.springframework.mail.MailException e) {
	        	log.error("Email Service Testing Failed.  Please check if the Port security SMTP port is whitelist by network security" );
	        	log.error("Email Service Testing Failed.  Please check SMTP Host, Username, Password and Port 443 & 587" );
	        	log.error(e.getMessage());
	        	System.exit(0);

		}
        
    }
}