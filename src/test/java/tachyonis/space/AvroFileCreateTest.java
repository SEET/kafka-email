package tachyonis.space;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.io.Writer;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericContainer;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.io.JsonEncoder;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecord;
import org.apache.avro.specific.SpecificRecordBase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tachyonis.space.email.EmailService;
import tachyonis.space.kafka.MailConsumer;
 
public class AvroFileCreateTest 
{
	private static final Logger log = LoggerFactory.getLogger(AvroFileCreateTest.class);
    @SuppressWarnings("resource")
	public static void main(String[] args) throws Exception 
    {
        //Create the application context
        AvroEml e = new AvroEml();
        List<CharSequence> l = Arrays.asList("ABC.com", "bar");
        e.setAttachment(l);
        e.setBody("Body");
        e.setId("Seet");
        e.setReceiver("RECE");
        e.setSendDate("SendDate1230");
        e.setSender("Sender");
        e.setSubject("Subject");

        String s = getJsonString(e);
        log.info(s);
        LockWrite("C:\\tmp\\test.json",s);
        serialize("C:\\tmp\\test.avro",e);
        System.exit(0);
    }
    public static void serialize(String fileName, AvroEml e) {
    	DatumWriter<AvroEml> userDatumWriter = new SpecificDatumWriter<AvroEml>(AvroEml.class);
    	DataFileWriter<AvroEml> dataFileWriter = new DataFileWriter<AvroEml>(userDatumWriter);
    	try {
			dataFileWriter.create(e.getSchema(), new File(fileName));
			dataFileWriter.append(e);
	    	dataFileWriter.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    }
    public static AvroEml jsontoavro(String fileName, String avrofileName) {
    	ObjectMapper objectMapper = new ObjectMapper();

    	File file = new File(fileName);
    	AvroEml eml = new AvroEml();
    	try {
			JsonEml json = objectMapper.readValue(file, JsonEml.class);
			eml.setId(UUID.randomUUID().toString());
			eml.setAttachment(json.getAttachment());
			eml.setBody(json.getBody());
			eml.setReceiver(json.getReceiver());
			eml.setSendDate(json.getSendDate());
			eml.setSender(json.getSender());
			eml.setSubject(json.getSender());
			return eml;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return eml;
    	
    }
    
    public void WriteVerify(String fileName, String value) 
    		  throws IOException {
    		    FileOutputStream fos = new FileOutputStream(fileName);
    		    DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));
    		    outStream.writeUTF(value);
    		    outStream.close();
    		 
    		    // verify the results
    		    String result;
    		    FileInputStream fis = new FileInputStream(fileName);
    		    DataInputStream reader = new DataInputStream(fis);
    		    result = reader.readUTF();
    		    reader.close();
    		 
    		    assertEquals(value, result);
    }   
    
    public static void Write(String fileName, String value) 
  		  throws IOException {
  		    FileOutputStream fos = new FileOutputStream(fileName);
  		    DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));
  		    outStream.writeUTF(value);
  		    outStream.close();
    }  
    
    public static void LockWrite(String fileName, String value) 
    		  throws IOException {
    		    RandomAccessFile stream = new RandomAccessFile(fileName, "rw");
    		    FileChannel channel = stream.getChannel();
    		 
    		    FileLock lock = null;
    		    try {
    		        lock = channel.tryLock();
    		    } catch (final OverlappingFileLockException e) {
    		        stream.close();
    		        channel.close();
    		    }
    		    stream.writeChars(value);
    		    lock.release();
    		 
    		    stream.close();
    		    channel.close();
    		}
    
    public static String getJsonString(GenericContainer record) throws IOException {
    	  ByteArrayOutputStream os = new ByteArrayOutputStream();
    	  JsonEncoder encoder = EncoderFactory.get().jsonEncoder(record.getSchema(), os);
    	  DatumWriter<GenericContainer> writer = new GenericDatumWriter<GenericContainer>();
    	  if (record instanceof SpecificRecord) {
    	    writer = new SpecificDatumWriter<GenericContainer>();
    	  }

    	  writer.setSchema(record.getSchema());
    	  writer.write(record, encoder);
    	  encoder.flush();
    	  String jsonString = new String(os.toByteArray(), Charset.forName("UTF-8"));
    	  os.close();
    	  return jsonString;
    	}
    
}